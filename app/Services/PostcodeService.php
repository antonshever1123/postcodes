<?php

namespace App\Services;

use App\Models\Postcode;
use GuzzleHttp\Client as Guzzle;

class PostcodeService
{
    /**
     * Search postcode by name
     *
     * @param  string  $name
     * @return Illuminate\Support\Facades\Redirect
     */
    public function search($name)
    {
        $postcode = Postcode::wherePostcode($name)
            ->firstOr(function () use ($name) {
                return $this->getPostcodeApi($name);
            });

        if ($postcode instanceof Postcode) {
            return redirect()->route('postcodes.show', $postcode);
        } else {
            return back();
        }
    }

    /**
     * Get postcode from the Postcodes.io then save to db.
     *
     * @param  string $postcode
     * @return mixed bool|App\Models\Postcode
     */
    private function getPostcodeApi($postcode)
    {
        $response = (new Guzzle(['http_errors' => false]))
            ->request('GET', 'https://api.postcodes.io/postcodes/' . $postcode);

        $data = json_decode($response->getBody(), true);

        if ($response->getStatusCode() == 200 && !empty($data['result'])) {
            return $this->save(
                array_merge($data['result'], compact('postcode'))
            );
        } else {
            session()->flash('api_error', $data['error'] ?? 'Some error');
        }

        return false;
    }

    /**
     * Save to db
     * 
     * @param  array $data
     * @return App\Models\Postcode
     */
    private function save($data)
    {
        return Postcode::create($data);
    }
}
