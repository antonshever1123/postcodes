<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\PostcodeService;

class PostcodeServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('postcode', function ($app) {
            return new PostcodeService;
        });
    }
}
