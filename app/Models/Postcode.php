<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Postcode extends Model
{
	public $timestamps = false;
	protected $fillable = [
		'postcode', 'country', 'region'
	];
}
