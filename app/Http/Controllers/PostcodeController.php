<?php

namespace App\Http\Controllers;

use App\Models\Postcode;
use App\Http\Requests\PostcodeRequest;

class PostcodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $postcodes = Postcode::all();
        return view('postcode.index', compact('postcodes'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Postcode  $postcode
     * @return \Illuminate\Http\Response
     */
    public function show(Postcode $postcode)
    {
        return view('postcode.show', compact('postcode'));
    }

    /**
     * Search postcode by name
     *
     * @param  PostcodeRequest  $request
     * @return mixed \Illuminate\Http\Response|Illuminate\Support\Facades\Redirect
     */
    public function search(PostcodeRequest $request)
    {
        // Illuminate\Support\ServiceProvider
        return app('postcode')->search($request->postcode);
    }
}
