<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PostcodeController@index');
Route::post('/search', 'PostcodeController@search')->name('postcodes.search');
Route::resource('postcodes', 'PostcodeController', ['only' => ['show']]);
