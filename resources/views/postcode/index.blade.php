@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Postcodes</div>
                    <div class="card-body">
                        {{ Form::open(['route' => 'postcodes.search', 'method' => 'POST']) }}
                            <div class="form-group">
                                {{ Form::label('postcode', 'Postcode') }}
                                {{ Form::text('postcode', null, ['class' => 'form-control', 'placeholder' => 'Enter postcode', 'required']) }}

                                @if ($errors->has('postcode'))
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ $errors->first('postcode') }}</strong>
                                    </span>
                                @endif

                                @if (session()->has('api_error'))
                                    <span class="invalid-feedback d-block" role="alert">
                                        <strong>{{ session('api_error') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                {{ Form::submit('Find', ['class' => 'btn btn-primary']) }}
                            </div>
                        {{ Form::close() }}
                        <hr>
                        @foreach($postcodes as $postcode)
                            <a href="{{ route('postcodes.show', $postcode) }}">{{ $postcode->postcode }}</a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection