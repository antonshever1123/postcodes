@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Postcode - {{ $postcode->postcode }}</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">{{ $postcode->postcode }}</div>
                            <div class="col-md-4">{{ $postcode->country }}</div>
                            <div class="col-md-4">{{ $postcode->region }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection